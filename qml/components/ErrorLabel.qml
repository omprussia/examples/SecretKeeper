// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

Item {
    property alias iconSource: iconView.source
    property alias text: textField.text

    objectName: "errorLabel"
    anchors {
        left: parent.left
        right: parent.right
    }
    implicitHeight: Math.max(iconView.height, textField.height)
    height: implicitHeight

    Image {
        id: iconView

        objectName: "iconView"
        anchors {
            top: parent.top
            left: parent.left
            leftMargin: Theme.horizontalPageMargin
        }
        source: Qt.resolvedUrl("image://theme/icon-lock-warning")
        sourceSize {
            width: Theme.iconSizeMedium
            height: Theme.iconSizeMedium
        }
    }

    Label {
        id: textField

        objectName: "textField"
        anchors {
            left: iconView.right
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }
        wrapMode: Label.WordWrap
        topPadding: 0
    }
}
