// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

Column {
    property bool needConfirm: true
    property int minimumLenght: dataStorage.minPasswordLenght()
    readonly property bool valid: password.length >= minimumLenght
    readonly property string password: passwordField.text.length < minimumLenght
                                       ? ""
                                       : (!needConfirm
                                          ? passwordField.text
                                          : (passwordField.text === passwordConfirmField.text
                                             ? passwordField.text
                                             : ""))

    function reset() {
        passwordField.text = qsTr("");
        passwordConfirmField.text = qsTr("");
    }

    objectName: "PasswordFields"
    anchors {
        left: parent.left
        right: parent.right
    }
    spacing: Theme.paddingLarge

    PasswordField {
        id: passwordField

        objectName: "passwordField"
        anchors {
            left: parent.left
            right: parent.right
        }
        horizontalAlignment: Text.AlignLeft
        inputMethodHints: Qt.ImhNoPredictiveText
        label: qsTr("Enter your password")
        placeholderText: label
        color: text.length < minimumLenght ? Theme.errorColor : palette.primaryColor
        cursorColor: color
        placeholderColor: color
        focus: true
        EnterKey.enabled: text.length >= minimumLenght
        EnterKey.iconSource: needConfirm ? "image://theme/icon-m-enter-next" : "image://theme/icon-m-enter-accept"

        EnterKey.onClicked: needConfirm ? passwordConfirmField.focus = true : focus = false
    }

    PasswordField {
        id: passwordConfirmField

        objectName: "passwordConfirmField"
        anchors {
            left: parent.left
            right: parent.right
        }
        horizontalAlignment: Text.AlignLeft
        inputMethodHints: Qt.ImhNoPredictiveText
        label: qsTr("Confirm your password")
        placeholderText: label
        color: (passwordField.text !== text || text.length < minimumLenght) ? Theme.errorColor : palette.primaryColor
        cursorColor: color
        placeholderColor: color
        visible: needConfirm
        EnterKey.enabled: text.length >= minimumLenght

        EnterKey.onClicked: focus = false
    }
}
