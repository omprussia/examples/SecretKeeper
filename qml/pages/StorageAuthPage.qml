// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    objectName: "storageAuthPage"

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }
            bottomPadding: Theme.paddingLarge
            spacing: Theme.paddingLarge

            PageHeader {
                objectName: "pageHeader"
                title: appWindow.appName
                extraContent.children: [
                    IconButton {
                        objectName: "pageHeaderButton"
                        anchors.verticalCenter: parent.verticalCenter
                        icon {
                            source: "image://theme/icon-m-about"
                            sourceSize {
                                width: Theme.iconSizeMedium
                                height: Theme.iconSizeMedium
                            }
                        }

                        onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                    }
                ]
            }

            Loader {
                objectName: "componentsLoader"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                sourceComponent: dataStorage.available ? cryptoIsAvailableComponent
                                                       : cryptoIsNotAvailableComponent
            }
        }
    }

    Component {
        id: cryptoIsNotAvailableComponent

        Label {
            objectName: "notAvailableLabel"
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.horizontalPageMargin
            }
            wrapMode: Text.WordWrap
            text: qsTr("The cryptographic container is not available.")
        }
    }

    Component {
        id: cryptoIsAvailableComponent

        Column {
            objectName: "column"
            spacing: Theme.paddingLarge

            Label {
                id: descriptionField

                objectName: "textArea"
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.horizontalPageMargin
                }
                wrapMode: Text.WordWrap
                text: dataStorage.created ? qsTr("The cryptographic container has been created. "
                                                 + "Enter the password to continue.")
                                          : qsTr("The cryptographic container has not been created. "
                                                 + "The minimum password length is 6 characters. "
                                                 + "To create it, set and confirm the password.")
            }

            PasswordFields {
                id: passwordFields

                objectName: "passwordFields"
                needConfirm: !dataStorage.created
                minimumLenght: dataStorage.minPasswordLenght()
            }

            ErrorLabel {
                id: errorField

                objectName: "errorField"
                height: implicitHeight
                visible: text.length !== 0
            }

            ButtonLayout {
                objectName: "buttonsLayout"

                Button {
                    objectName: "openCreateButton"
                    text: dataStorage.created ? qsTr("Open") : qsTr("Create")
                    enabled: passwordFields.valid

                    onClicked: {
                        var created = dataStorage.created;
                        if (!created) {
                            created = dataStorage.create(passwordFields.password);
                        }
                        if (!created) {
                            errorField.text = qsTr("Failed to create a cryptographic container. "
                                                   + "Please try again with another password.");
                            passwordFields.reset();
                            return;
                        }
                        var opened = dataStorage.opened;
                        if (!opened) {
                            opened = dataStorage.open(passwordFields.password);
                        }
                        if (!opened) {
                            errorField.text = qsTr("You entered the wrong password. "
                                                   + "Please try again with another password.");
                            passwordFields.reset();
                            return;
                        }
                        pageStack.replace(Qt.resolvedUrl("RecordsViewerPage.qml"), {
                                "storagePassword": passwordFields.password
                            }, PageStackAction.Immediate);
                    }
                }
            }
        }
    }
}
