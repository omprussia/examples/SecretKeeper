// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
    property string resource
    property string login
    property string password
    property bool editing: true

    objectName: "recordFieldsPage"
    canAccept: {
        var notEmpty = resourceField.text.length > 0 && loginField.text.length > 0 && passwordField.text.length > 0;
        var changed = resource !== resourceField.text || login !== loginField.text || password !== passwordField.text;
        return editing ? notEmpty && changed : notEmpty;
    }

    onAccepted: {
        resource = resourceField.text;
        login = loginField.text;
        password = passwordField.text;
        pageStack.pop();
    }

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                acceptText: editing ? qsTr("Edit") : qsTr("Create")
                cancelText: qsTr("Back")
            }

            TextField {
                id: resourceField

                objectName: "resourceField"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: Text.AlignLeft
                inputMethodHints: Qt.ImhNoPredictiveText
                label: editing ? qsTr("Edit resource name") : qsTr("Enter resource name")
                placeholderText: label
                focus: true
                text: resource
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"

                EnterKey.onClicked: loginField.focus = true
            }

            TextField {
                id: loginField

                objectName: "loginField"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: Text.AlignLeft
                inputMethodHints: Qt.ImhNoPredictiveText
                label: editing ? qsTr("Edit login") : qsTr("Enter login")
                placeholderText: label
                text: login
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"

                EnterKey.onClicked: passwordField.focus = true
            }

            PasswordField {
                id: passwordField

                objectName: "passwordField"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: Text.AlignLeft
                inputMethodHints: Qt.ImhNoPredictiveText
                label: editing ? qsTr("Edit password") : qsTr("Enter password")
                placeholderText: label
                text: password
                EnterKey.enabled: text.length > 0

                EnterKey.onClicked: focus = false
            }

            ButtonLayout {
                objectName: "buttonsLayout"

                Button {
                    objectName: "generateButton"
                    text: qsTr("Generate")

                    onClicked: passwordField.text = dataStorage.generatePassword()
                }

                Button {
                    objectName: "copyButton"
                    text: qsTr("Copy")

                    onClicked: Clipboard.text = passwordField.text
                }
            }
        }
    }
}
