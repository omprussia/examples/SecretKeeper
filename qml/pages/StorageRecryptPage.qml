// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Dialog {
    property string storagePassword

    objectName: "storageRecryptPage"
    canAccept: passwordFields.valid

    onAccepted: {
        storagePassword = passwordFields.password;
        pageStack.pop();
    }

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Column {
            id: column

            objectName: "column"
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: Theme.paddingLarge

            DialogHeader {
                objectName: "pageHeader"
                acceptText: qsTr("Apply")
                cancelText: qsTr("Back")
            }

            Label {
                id: descriptionField

                objectName: "descriptionField"
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.horizontalPageMargin
                }
                wrapMode: Text.WordWrap
                text: qsTr("You cannot use previous passwords.")
            }

            PasswordFields {
                id: passwordFields

                objectName: "passwordFields"
                needConfirm: true
                minimumLenght: dataStorage.minPasswordLenght()
            }
        }
    }
}
