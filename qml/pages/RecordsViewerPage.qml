// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0
import ru.auroraos.SecretKeeper 1.0

Page {
    property string storagePassword

    objectName: "recordsViewerPage"

    SilicaListView {
        objectName: "listView"
        anchors.fill: parent
        clip: true
        currentIndex: -1
        header: viewHeaderComponent
        delegate: viewDelegateComponent
        model: viewModel

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }

        Notification {
            id: notification

            objectName: "notification"
            appName: appWindow.appName
            summary: qsTr("Recrypt Storage")
            body: qsTr("Failed to change password. Try another one.")
            urgency: Notification.Critical
            isTransient: true

            Component.onDestruction: notification.close()
        }

        PullDownMenu {
            objectName: "pullDownMenu"

            MenuItem {
                objectName: "recryptMenuItem"
                text: qsTr("Recrypt Storage")

                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("StorageRecryptPage.qml"));
                    dialog.accepted.connect(function () {
                            var opened = dataStorage.opened;
                            if (opened) {
                                dataStorage.close();
                            }
                            var storagePasswordNew = dialog.storagePassword;
                            if (dataStorage.recrypt(storagePassword, storagePasswordNew)) {
                                storagePassword = storagePasswordNew;
                            } else {
                                notification.publish();
                            }
                            opened = dataStorage.opened;
                            if (!opened) {
                                dataStorage.open(storagePassword);
                            }
                        });
                }
            }

            MenuItem {
                objectName: "removeStorageMenuItem"
                text: qsTr("Remove Storage")

                onClicked: {
                    var opened = dataStorage.opened;
                    if (opened) {
                        dataStorage.close();
                    }
                    var created = dataStorage.created;
                    if (created) {
                        created = !dataStorage.remove();
                    }
                    if (!created) {
                        pageStack.replace(Qt.resolvedUrl("StorageAuthPage.qml"), {}, PageStackAction.Immediate);
                    }
                }
            }

            MenuItem {
                objectName: "addRecordMenuItem"
                text: qsTr("Add Record")

                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("RecordFieldsPage.qml"), {
                            "editing": false
                        });
                    dialog.accepted.connect(function () {
                            viewModel.insert(dialog.resource, dialog.login, dialog.password);
                        });
                }
            }
        }
    }

    DataModel {
        id: viewModel

        objectName: "viewModel"
        storagePath: dataStorage.path
    }

    Component {
        id: viewHeaderComponent

        SearchField {
            id: searchField

            objectName: "searchField"
            anchors {
                left: parent.left
                right: parent.right
            }
            placeholderText: qsTr("Search")

            onTextChanged: viewModel.filter = text
        }
    }

    Component {
        id: viewDelegateComponent

        ListItem {
            id: viewDelegateItem

            function editRecord() {
                var dialog = pageStack.push(Qt.resolvedUrl("RecordFieldsPage.qml"), {
                        "resource": model.resource,
                        "login": model.login,
                        "password": model.password,
                        "editing": true
                    });
                dialog.accepted.connect(function () {
                        viewModel.update(model.resource, dialog.resource, dialog.login, dialog.password);
                    });
            }

            function removeRecord() {
                remorseAction(qsTr("Deleting"), function () {
                        viewModel.remove(model.resource);
                    });
            }

            objectName: "viewDelegateItem"
            menu: contextMenu
            contentHeight: viewDelegateItemRow.height + Theme.horizontalPageMargin * 2

            ListView.onAdd: animateRemoval(viewDelegateItem)
            ListView.onRemove: animateRemoval(viewDelegateItem)
            onClicked: editRecord()

            Item {
                id: viewDelegateItemRow

                objectName: "viewDelegateItemRow"
                anchors.verticalCenter: parent.verticalCenter
                height: Math.max(viewDelegateItemIcon.height, viewDelegateItemColumn.height)

                Image {
                    id: viewDelegateItemIcon

                    objectName: "viewDelegateItemIcon"
                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: Theme.horizontalPageMargin
                    }
                    source: Qt.resolvedUrl("image://theme/icon-m-keys")
                    sourceSize {
                        width: Theme.iconSizeMedium
                        height: Theme.iconSizeMedium
                    }
                }

                Column {
                    id: viewDelegateItemColumn

                    objectName: "viewDelegateItemColumn"
                    anchors {
                        left: viewDelegateItemIcon.right
                        right: parent.right
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.horizontalPageMargin
                    }
                    spacing: Theme.paddingSmall

                    Label {
                        objectName: "resourceLabel"
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        height: implicitHeight
                        leftPadding: Theme.paddingMedium
                        rightPadding: Theme.paddingMedium
                        text: qsTr("Resource '%1'").arg(model.resource)
                    }

                    Label {
                        objectName: "loginLabel"
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        height: implicitHeight
                        leftPadding: Theme.paddingMedium
                        rightPadding: Theme.paddingMedium
                        text: qsTr("login: %1").arg(model.login)
                        color: viewDelegateItem.highlighted ? palette.highlightColor : palette.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                    }
                }
            }

            Component {
                id: contextMenu

                ContextMenu {
                    objectName: "contextMenu"

                    MenuItem {
                        objectName: "editMenuItem"
                        text: qsTr("Edit")

                        onClicked: viewDelegateItem.editRecord()
                    }

                    MenuItem {
                        objectName: "removeMenuItem"
                        text: qsTr("Remove")

                        onClicked: viewDelegateItem.removeRecord()
                    }
                }
            }
        }
    }
}
