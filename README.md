# Secret Keeper

The project provides an example of using crypto-containers via SStore API and QML models to
manage sensitive data.

The main purpose is to show not only what features are available to work with crypto-containers,
but also how to use them correctly.

Build status:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/SecretKeeper/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/SecretKeeper/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/SecretKeeper/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/SecretKeeper/-/commits/dev)

**Pay attention. The crypto container only works in the sandbox.**

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.SecretKeeper.pro](ru.auroraos.SecretKeeper.pro)** file describes the project structure for the qmake build system.
* **[icons](icons)** directory contains the application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[components](qml/components)** derectory contains the custom UI components.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[images](qml/images)** directory contains the additional custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[SecretKeeper.qml](qml/SecretKeeper.qml)** file provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.SecretKeeper.spec](rpm/ru.auroraos.SecretKeeper.spec)** file is used by rpmbuild tool.
* **[src](src)** directory contains the C++ source code.
  * **[model](src/model)** directory contains data model.
  * **[storage](src/storage)** directory contains the implementation data storage.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[ru.auroraos.SecretKeeper.desktop](ru.auroraos.SecretKeeper.desktop)** file defines the display and parameters for launching the application.

## Compatibility

The project is compatible with all the current versions of the Aurora OS.

- Branch `example_4.02.173`: application with SailfishApp::application instance, it is used for OS versions
down to and including the 4.02.173

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
