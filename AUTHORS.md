# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2021-2022
* Pavel Kazeko, <p.kazeko@omp.ru>
  * Developer, 2022
* Vasiliy Rychkov, <v.rychkov@omp.ru>
  * Icon Designer, 2022
* Alexey Andreev, <a.andreev@omp.ru>
  * Reviewer, 2022
* Vladislav Larionov
  * Reviewer, 2023
* Andrej Begichev, <a.begichev@omp.ru>
  * Maintainer, 2023
  * Developer, 2023
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
