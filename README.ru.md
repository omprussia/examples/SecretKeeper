# Secret Keeper

Проект предоставляет пример использования криптоконтейнеров через SStore API и QML модели для управления
чувствительными данными.

Главная цель - показать не только то, какие возможности доступны для работы с криптоконтейнерами, но и как правильно их использовать.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/SecretKeeper/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/SecretKeeper/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/SecretKeeper/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/SecretKeeper/-/commits/dev)

**Внимание. Криптоконтейнер работает только в песочнице.**

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.SecretKeeper.pro](ru.auroraos.SecretKeeper.pro)** описывает структуру проекта для системы сборки qmake.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
  * Каталог **[components](qml/components)** содержит пользовательские компоненты пользовательского интерфейса.
  * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
  * Каталог **[icons](qml/icons)** содержит дополнительные иконки интерфейса пользователя.
  * Каталог **[pages](qml/pages)** содержит страницы приложения.
  * Файл **[SecretKeeper.qml](qml/SecretKeeper.qml)** предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
  * Файл **[ru.auroraos.SecretKeeper.spec](rpm/ru.auroraos.SecretKeeper.spec)** используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
  * Каталог **[model](src/model)** содержит модель данных.
  * Каталог **[storage](src/storage)** содержит реализацию хранилища данных.
  * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.SecretKeeper.desktop](ru.auroraos.SecretKeeper.desktop)** определяет отображение и параметры запуска приложения.

## Совместимость

Проект совместим с актуальными версиями ОС Аврора.

- Ветка `example_4.02.173` - приложение с экземпляром SailfishApp::application, которое используется для версии ОС 4.02.173 и ниже.

## Снимки экранов

![screenshots](screenshots/screenshots.png)

## This document in English

- [README.md](README.md)
