# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.SecretKeeper

QT += dbus sql

message(SDK VERSION: $${AURORA_SDK_VERSION})
message(SDK MAJOR VERSION: $${AURORA_SDK_MAJOR_VERSION})
message(SDK MINOR VERSION: $${AURORA_SDK_MINOR_VERSION})
message(SDK BUILD VERSION: $${AURORA_SDK_BUILD_VERSION})
message(SDK REVSION VERSION: $${AURORA_SDK_REVISION_VERSION})

# For old version sdk(os) have to use sailfishapp and added define USING_SAILFISHAPP
lessThan(AURORA_SDK_VERSION, 4.0.2.372) {
    USING_SAILFISHAPP=ON
    DEFINES+=USING_SAILFISHAPP
}

equals(USING_SAILFISHAPP, "ON") {
    CONFIG += \
        sailfishapp \
        sailfishapp_i18n \
} else {
    CONFIG += \
        auroraapp \
        auroraapp_i18n \
}

TRANSLATIONS += \
    translations/ru.auroraos.SecretKeeper.ts \
    translations/ru.auroraos.SecretKeeper-ru.ts \

HEADERS += \
    src/model/datamodel.h \
    src/storage/datastorage.h \

SOURCES += \
    src/model/datamodel.cpp \
    src/storage/datastorage.cpp \
    src/main.cpp \

DISTFILES += \
    rpm/ru.auroraos.SecretKeeper.spec \
    translations/*.ts \
    ru.auroraos.SecretKeeper.desktop \
    LICENSE.BSD-3-Clause.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    AUTHORS.md \
    README.md \
    README.ru.md \

equals(USING_SAILFISHAPP, "ON") {
    SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172
} else {
    AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
}
