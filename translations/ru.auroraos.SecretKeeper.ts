<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>The 3-Clause BSD License</source>
        <translation>The 3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="41"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;The project provides an example of using crypto-containers and QML
                            models to manage sensitive data.&lt;/p&gt;
                            &lt;p&gt;The main purpose is to show not only what features are available to
                            work with crypto-containers, but also how to use them correctly.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2022 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PasswordFields</name>
    <message>
        <location filename="../qml/components/PasswordFields.qml" line="13"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/PasswordFields.qml" line="41"/>
        <source>Enter your password</source>
        <translation>Enter your password</translation>
    </message>
    <message>
        <location filename="../qml/components/PasswordFields.qml" line="63"/>
        <source>Confirm your password</source>
        <translation>Confirm your password</translation>
    </message>
</context>
<context>
    <name>RecordFieldsPage</name>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="49"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="62"/>
        <source>Enter resource name</source>
        <translation>Enter resource name</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="82"/>
        <source>Enter login</source>
        <translation>Enter login</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="101"/>
        <source>Enter password</source>
        <translation>Enter password</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="48"/>
        <source>Create</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="114"/>
        <source>Generate</source>
        <translation>Generate</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="121"/>
        <source>Copy</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="48"/>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="62"/>
        <source>Edit resource name</source>
        <translation>Edit resource name</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="82"/>
        <source>Edit login</source>
        <translation>Edit login</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordFieldsPage.qml" line="101"/>
        <source>Edit password</source>
        <translation>Edit password</translation>
    </message>
</context>
<context>
    <name>RecordsViewerPage</name>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="121"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="146"/>
        <source>Deleting</source>
        <translation>Deleting</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="203"/>
        <source>Resource &apos;%1&apos;</source>
        <translation>Resource &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="215"/>
        <source>login: %1</source>
        <translation>login: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="230"/>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="237"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="32"/>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="45"/>
        <source>Recrypt Storage</source>
        <translation>Recrypt Storage</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="70"/>
        <source>Remove Storage</source>
        <translation>Remove Storage</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="89"/>
        <source>Add Record</source>
        <translation>Add Record</translation>
    </message>
    <message>
        <location filename="../qml/pages/RecordsViewerPage.qml" line="33"/>
        <source>Failed to change password. Try another one.</source>
        <translation>Failed to change password. Try another one.</translation>
    </message>
</context>
<context>
    <name>SecretKeeper</name>
    <message>
        <location filename="../qml/SecretKeeper.qml" line="11"/>
        <source>Secret Keeper</source>
        <translation>Secret Keeper</translation>
    </message>
</context>
<context>
    <name>StorageAuthPage</name>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="74"/>
        <source>The cryptographic container is not available.</source>
        <translation>The cryptographic container is not available.</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="95"/>
        <source>The cryptographic container has been created. Enter the password to continue.</source>
        <translation>The cryptographic container has been created. Enter the password to continue.</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="97"/>
        <source>The cryptographic container has not been created. The minimum password length is 6 characters. To create it, set and confirm the password.</source>
        <translation>The cryptographic container has not been created. The minimum password length is 6 characters. To create it, set and confirm the password.</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="123"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="123"/>
        <source>Create</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="132"/>
        <source>Failed to create a cryptographic container. Please try again with another password.</source>
        <translation>Failed to create a cryptographic container. Please try again with another password.</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageAuthPage.qml" line="142"/>
        <source>You entered the wrong password. Please try again with another password.</source>
        <translation>You entered the wrong password. Please try again with another password.</translation>
    </message>
</context>
<context>
    <name>StorageRecryptPage</name>
    <message>
        <location filename="../qml/pages/StorageRecryptPage.qml" line="41"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageRecryptPage.qml" line="40"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/pages/StorageRecryptPage.qml" line="54"/>
        <source>You cannot use previous passwords.</source>
        <translation>You cannot use previous passwords.</translation>
    </message>
</context>
</TS>
