// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QDateTime>
#include <QtCore/QVector>
#include <QtDBus/QDBusConnectionInterface>
#include <random>

#include "datastorage.h"

const QString c_sstoreDBusServiceName{ QStringLiteral("ru.omprussia.sstore") };
const QString c_sstoreDBusObjectPath{ QStringLiteral("/ru/omprussia/sstore") };
const QString c_sstoreDBusInterfaceName{ c_sstoreDBusServiceName };
const qint32 c_sstoreSuccessCode{ 0 };
const qint32 c_sstoreMinPassLenght{ 6 };

DataStorage::DataStorage(QObject *parent)
    : QObject(parent),
      m_dbusInterface(new QDBusInterface(c_sstoreDBusServiceName, c_sstoreDBusObjectPath,
                                         c_sstoreDBusInterfaceName, QDBusConnection::systemBus()))
{
    std::mt19937(QDateTime::currentDateTime().toTime_t());

    m_dbusInterface->setTimeout(0x7FFFFFFF);
}

bool DataStorage::available()
{
    return m_dbusInterface->connection()
            .interface()
            ->isServiceRegistered(c_sstoreDBusServiceName)
            .value();
}

bool DataStorage::created()
{
    auto message = m_dbusInterface->call(QStringLiteral("Exists"));
    if ((message.type() != QDBusMessage::ReplyMessage)
        || (message.arguments().count() != 1
            || message.arguments().at(0).type() != QVariant::Bool)) {
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());
        return false;
    }

    return message.arguments().at(0).toBool();
}

bool DataStorage::create(const QString &password)
{
    auto message = m_dbusInterface->call(QStringLiteral("Create"), password);
    if ((message.type() != QDBusMessage::ReplyMessage)
        || (message.arguments().count() != 1
            || message.arguments().at(0).type() != QVariant::Int)) {
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());
        return false;
    }

    emit createdChanged(created());

    return message.arguments().at(0).toInt() == c_sstoreSuccessCode;
}

bool DataStorage::remove()
{
    auto message = m_dbusInterface->call(QStringLiteral("Delete"));
    if ((message.type() != QDBusMessage::ReplyMessage)
        || (message.arguments().count() != 1
            || message.arguments().at(0).type() != QVariant::Int)) {
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());
        return false;
    }

    emit createdChanged(created());

    return message.arguments().at(0).toInt() == c_sstoreSuccessCode;
}

bool DataStorage::recrypt(const QString &passwordOld, const QString &passwordNew)
{
    auto message = m_dbusInterface->call(QStringLiteral("Recrypt"), passwordOld, passwordNew);
    if ((message.type() != QDBusMessage::ReplyMessage)
        || (message.arguments().count() != 1
            || message.arguments().at(0).type() != QVariant::Int)) {
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());
        return false;
    }

    return message.arguments().at(0).toInt() == c_sstoreSuccessCode;
}

bool DataStorage::opened()
{
    return !m_path.isNull();
}

bool DataStorage::open(const QString &password)
{
    auto message = m_dbusInterface->call(QStringLiteral("Open"), password);
    if ((message.type() != QDBusMessage::ReplyMessage)
        || (message.arguments().count() != 2 || message.arguments().at(0).type() != QVariant::Int
            || message.arguments().at(1).type() != QVariant::String)) {
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());
        return false;
    }

    bool result = message.arguments().at(0).toInt() == c_sstoreSuccessCode;
    if (result) {
        auto path = message.arguments().at(1).toString();
        if (m_path != path) {
            m_path = message.arguments().at(1).toString();
            emit openedChanged(opened());
            emit pathChanged(m_path);
        }
    }

    return result;
}

void DataStorage::close()
{
    auto message = m_dbusInterface->call(QStringLiteral("Close"));
    if (message.type() != QDBusMessage::ReplyMessage || message.arguments().count() != 0)
        qWarning("%s: %s", message.errorName().toUtf8().data(),
                 message.errorMessage().toUtf8().data());

    m_path.clear();

    emit pathChanged(m_path);
    emit openedChanged(opened());
}

QString DataStorage::path() const
{
    return m_path;
}

QString DataStorage::generatePassword(qint32 length)
{
    static QVector<QChar> lowercase{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                                     'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                                     's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
    static QVector<QChar> uppercase{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                     'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                                     'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    static QVector<QChar> digits{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    static QVector<QChar> specialchars{ '&', '~', '#', '-', '_', '$', '%', '*', '!', '?' };
    static QVector<QChar> symbols{ lowercase + uppercase + digits + specialchars };

    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(0, symbols.length() - 1);
    if (length == -1)
        length = c_sstoreMinPassLenght + distribution(generator) % 6;

    QString password;
    for (qint32 i = 0; i < length; ++i)
        password += symbols.at(distribution(generator));

    return password;
}

qint32 DataStorage::minPasswordLenght()
{
    return c_sstoreMinPassLenght;
}
