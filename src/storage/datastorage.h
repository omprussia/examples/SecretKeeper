// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtDBus/QDBusInterface>

class DataStorage : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ available CONSTANT)
    Q_PROPERTY(bool created READ created NOTIFY createdChanged)
    Q_PROPERTY(bool opened READ opened NOTIFY openedChanged)
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)

public:
    explicit DataStorage(QObject *parent = nullptr);

    Q_INVOKABLE bool available();
    Q_INVOKABLE bool created();
    Q_INVOKABLE bool create(const QString &password);
    Q_INVOKABLE bool remove();
    Q_INVOKABLE bool recrypt(const QString &passwordOld, const QString &passwordNew);
    Q_INVOKABLE bool opened();
    Q_INVOKABLE bool open(const QString &password);
    Q_INVOKABLE void close();
    Q_INVOKABLE QString path() const;

    Q_INVOKABLE static QString generatePassword(qint32 length = -1);
    Q_INVOKABLE static qint32 minPasswordLenght();

signals:
    void createdChanged(bool created);
    void openedChanged(bool opened);
    void pathChanged(const QString &path);

private:
    QSharedPointer<QDBusInterface> m_dbusInterface{ nullptr };
    QString m_path{};
};

#endif // DATASTORAGE_H
