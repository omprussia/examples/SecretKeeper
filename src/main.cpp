// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QGuiApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#ifdef USING_SAILFISHAPP
#include <sailfishapp.h>
#else
#include <auroraapp.h>
#endif

#include "storage/datastorage.h"
#include "model/datamodel.h"

#ifdef USING_SAILFISHAPP
namespace LibApp = SailfishApp;
#else
namespace LibApp = Aurora::Application;
#endif

int main(int argc, char *argv[])
{
    qmlRegisterType<DataModel>("ru.auroraos.SecretKeeper", 1, 0, "DataModel");

    QScopedPointer<QGuiApplication> application(LibApp::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("SecretKeeper"));

    QScopedPointer<QQuickView> view(LibApp::createView());
    view->rootContext()->setContextProperty(QStringLiteral("dataStorage"),
                                            new DataStorage(view.data()));
    view->setSource(LibApp::pathTo(QStringLiteral("qml/SecretKeeper.qml")));
    view->show();

    return application->exec();
}
