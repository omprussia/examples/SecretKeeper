// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtCore/QDir>

#include "datamodel.h"

DataModel::DataModel(QObject *parent)
    : QSqlQueryModel(parent),
      m_dataBase(QSqlDatabase::addDatabase("QSQLITE")),
      m_storageName(QStringLiteral("records.db")),
      m_request(QStringLiteral("SELECT resource, login, password FROM records"))
{
}

QHash<int, QByteArray> DataModel::roleNames() const
{
    const static QHash<int, QByteArray> roleNames{
        { Qt::UserRole + 0, "resource" },
        { Qt::UserRole + 1, "login" },
        { Qt::UserRole + 2, "password" },
    };

    return roleNames;
}

QVariant DataModel::data(const QModelIndex &index, int role) const
{
    auto modelIndex = QSqlQueryModel::index(index.row(), role - Qt::UserRole);
    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

QString DataModel::storageName() const
{
    return m_storageName;
}

void DataModel::setStorageName(const QString &storageName)
{
    if (m_storageName == storageName)
        return;

    m_storageName = storageName;
    emit storageNameChanged(storageName);

    _updateDataBase();
}

QString DataModel::storagePath() const
{
    return m_storagePath;
}

void DataModel::setStoragePath(const QString &storagePath)
{
    if (m_storagePath == storagePath)
        return;

    m_storagePath = storagePath;
    emit storagePathChanged(storagePath);

    _updateDataBase();
}

QString DataModel::filter() const
{
    return m_filter;
}

void DataModel::setFilter(const QString &filter)
{
    if (m_filter == filter)
        return;

    m_filter = filter;
    emit filterChanged(filter);

    _updateModel();
}

bool DataModel::insert(const QString &resource, const QString &login, const QString &password)
{
    if (!m_dataBase.isOpen())
        return false;

    m_insertQuery.bindValue(QStringLiteral(":resource"), resource);
    m_insertQuery.bindValue(QStringLiteral(":login"), login);
    m_insertQuery.bindValue(QStringLiteral(":password"), password);

    auto result = m_insertQuery.exec();
    if (result) {
        _updateModel();
        emit inserted(resource, login, password);
    }

    return result;
}

bool DataModel::update(const QString &resource, const QString &resourceNew, const QString &loginNew,
                       const QString &passwordNew)
{
    if (!m_dataBase.isOpen())
        return false;

    m_updateQuery.bindValue(QStringLiteral(":resource"), resource);
    m_updateQuery.bindValue(QStringLiteral(":resourceNew"), resourceNew);
    m_updateQuery.bindValue(QStringLiteral(":loginNew"), loginNew);
    m_updateQuery.bindValue(QStringLiteral(":passwordNew"), passwordNew);

    auto result = m_updateQuery.exec();
    if (result) {
        _updateModel();
        emit updated(resource, resourceNew, loginNew, passwordNew);
    }

    return result;
}

bool DataModel::remove(const QString &resource)
{
    if (!m_dataBase.isOpen())
        return false;

    m_removeQuery.bindValue(QStringLiteral(":resource"), resource);

    auto result = m_removeQuery.exec();
    if (result) {
        _updateModel();
        emit removed(resource);
    }

    return result;
}

void DataModel::_updateDataBase()
{
    auto storageDir = QDir(m_storagePath);
    auto databaseName = storageDir.absoluteFilePath(m_storageName);

    if (m_dataBase.databaseName() == databaseName && m_dataBase.isOpen())
        return;

    if (m_dataBase.isOpen())
        m_dataBase.close();

    if (!storageDir.exists())
        if (!storageDir.mkpath(storageDir.absolutePath()))
            return;

    if (databaseName.isEmpty() || databaseName.isNull())
        return;

    m_dataBase.setDatabaseName(databaseName);
    if (m_dataBase.open()) {
        m_dataBase.exec(QStringLiteral("CREATE TABLE IF NOT EXISTS records "
                                       "(resource TEXT NOT NULL, "
                                       "login TEXT NOT NULL, "
                                       "password TEXT NOT NULL);"));

        m_insertQuery = QSqlQuery(m_dataBase);
        m_insertQuery.prepare(
                QStringLiteral("INSERT OR IGNORE INTO records (resource, login, password) "
                               "VALUES(:resource, :login, :password);"));
        m_updateQuery = QSqlQuery(m_dataBase);
        m_updateQuery.prepare(QStringLiteral("UPDATE OR IGNORE records "
                                             "SET resource = :resourceNew, "
                                             "login = :loginNew, "
                                             "password = :passwordNew "
                                             "WHERE resource = :resource;"));
        m_removeQuery = QSqlQuery(m_dataBase);
        m_removeQuery.prepare(QStringLiteral("DELETE FROM records "
                                             "WHERE resource = :resource;"));

        _updateModel();
    }
}

void DataModel::_updateModel()
{
    if (!m_dataBase.isOpen())
        return;

    auto query = m_request;
    if (!m_filter.isNull() && !m_filter.isEmpty())
        query += QStringLiteral(" WHERE resource LIKE \"%%1%\" OR login LIKE \"%%2%\"")
                         .arg(m_filter, m_filter);

    query += QStringLiteral(" ORDER BY rowid DESC;");

    setQuery(query, m_dataBase);
}
