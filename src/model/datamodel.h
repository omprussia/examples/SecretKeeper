// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QtCore/QByteArray>
#include <QtCore/QVariant>
#include <QtCore/QString>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>

class DataModel : public QSqlQueryModel
{
    Q_OBJECT

    Q_PROPERTY(QString storageName READ storageName WRITE setStorageName NOTIFY storageNameChanged)
    Q_PROPERTY(QString storagePath READ storagePath WRITE setStoragePath NOTIFY storagePathChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)

public:
    explicit DataModel(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE QString storageName() const;
    Q_INVOKABLE void setStorageName(const QString &storageName);

    Q_INVOKABLE QString storagePath() const;
    Q_INVOKABLE void setStoragePath(const QString &storagePath);

    Q_INVOKABLE QString filter() const;
    Q_INVOKABLE void setFilter(const QString &filter);

    Q_INVOKABLE bool insert(const QString &resource, const QString &login, const QString &password);
    Q_INVOKABLE bool update(const QString &resource, const QString &resourceNew,
                            const QString &loginNew, const QString &passwordNew);
    Q_INVOKABLE bool remove(const QString &resource);

signals:
    void storageNameChanged(const QString &storageName);
    void storagePathChanged(const QString &storagePath);
    void filterChanged(const QString &filter);
    void inserted(const QString &resource, const QString &login, const QString &password);
    void updated(const QString &resource, const QString &resourceNew, const QString &loginNew,
                 const QString &passwordNew);
    void removed(const QString &resource);

private:
    void _updateDataBase();
    void _updateModel();

private:
    QSqlDatabase m_dataBase{};
    QSqlQuery m_insertQuery{};
    QSqlQuery m_updateQuery{};
    QSqlQuery m_removeQuery{};
    QString m_storageName{};
    QString m_storagePath{};
    QString m_request{};
    QString m_filter{};
};

#endif // DATAMODEL_H
